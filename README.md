# Demo for Activity module on JupyterHub resources

[![Launch JupyterHub](https://img.shields.io/badge/activity-demo-orange?style=flat-square)](https://neur1630.jupyter.brown.edu/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.com%2Ffleischmann-lab%2Fcourses%2Fneur1630-fall2023%2Factivity-demo&urlpath=lab%2Ftree%2Factivity-demo%2F&branch=main)

Click on the icon above (or [here](https://neur1630.jupyter.brown.edu/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.com%2Ffleischmann-lab%2Fcourses%2Fneur1630-fall2023%2Factivity-demo&urlpath=lab%2Ftree%2Factivity-demo%2F&branch=main)) to launch Jupyter Hub.

You can also go to `notebooks` folder.

## Data sources

### daste-2023

- Source: these are from Dr. Fleischmann's lab, experiment by Simon Daste (graduate student). The data currently reside on [DANDI Archive](https://dandiarchive.org/dandiset/000167)

### pungor-etal-2023-octopus

- Source: Niell, Cristopher; Pungor, Judit (2023). Calcium imaging data from: Functional organization of visual responses in the octopus optic lobe [Dataset]. Dryad. https://doi.org/10.5061/dryad.c2fqz61f2
- Paper: Pungor et al 2023 (Current Biol). *Functional organization of visual responses in the octopus optic lobe*. https://doi.org/10.1016/j.cub.2023.05.069


### scherrer-etal-2023-feescope

- Source (only subsets): J. R. Scherrer and Galen F. Lynch and Jie J. Zhang and Michale S. Fee (2023). Feescope-Paper-Data. Data from <https://academictorrents.com/details/aa2a3d2f5ff0b3974871db47db57bc3dabf3c192> and accompanying Github URL (not downloaded) <https://github.com/FeeLab/Fee-Scopes>.
- Paper: Scherrer et al 2023. *An optical design enabling lightweight and large field-of-view head-mounted microscopes*. Nat Methods 20, 546–549 (2023). https://doi.org/10.1038/s41592-023-01806-1

